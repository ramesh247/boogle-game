#  @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 
class Api::GamesController < ApplicationController

  def index
    render json: { :payloads => [
      {
        :data => WELCOME,
        :status => OK,
        :message => WELCOME
      }
    ]
  }.to_json
  end

  def scores
    keyword = params[:keyword]
    if keyword.length < 3
      message = INVALID_LENGTH
      point = 0
    else
      is_valid_keyWord = DICTIONARY.include?(keyword)
      if is_valid_keyWord
        point = calculate_score keyword.length
        message = SUCCESS_MSG
      else
        point = 0
        message = INVALID_WORD
      end
    end

    render json: { :payloads => 
        {
          :data => {point: point},
          :status => OK,
          :message => message
        }
    }.to_json
  end

  def calculate_score (len)
    score_obj = SCORE.find {|l| l[:length] == len }
    if score_obj
      return score_obj[:point]
    else
      return 11
    end
  end
    
end
