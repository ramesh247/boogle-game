/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

import React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom'
import AppRoute from './appRoute';
import Layout from './components/layout';
import Home from './components/home';
import GameBoard from './components/game-board';
import PageNotFound from './components/page-not-found';

const routes = (
	<BrowserRouter>
		<Switch>
			<AppRoute exact path="/" layout={Layout} component={Home} />
			<AppRoute path="/play-game" layout={Layout} component={GameBoard} />
			<Route path="*" component={PageNotFound} />
		</Switch>
	</BrowserRouter>
);

export default routes;
