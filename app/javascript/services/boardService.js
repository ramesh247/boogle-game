/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

import * as httpService from './httpService';
import APICONSTANTS from '../constants/apiConstant';

export function scores(keyword) {
  return httpService.get(`${APICONSTANTS.GAME}/${keyword}`)
    .then(response => {
      return response.data
    })
    .catch(error => {
      throw error;
    })
}