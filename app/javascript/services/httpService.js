/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

import axios from 'axios';
import config from '../config';

const API_BASE_URL = config.apiBaseUrl[process.env.NODE_ENV || 'development'];

export function get(resourceName, params = {}) {
  let config = {
    method: 'get',
    url: `${API_BASE_URL}/${resourceName}`,
    params: params
  };
  return axios(config);
}
