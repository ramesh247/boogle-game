/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

export function generateBoard(length) {
	var result = '';
	var characters = 'APRSMASPRARASFRSAAEEEEAAFIRSADENNNAEEEEMAEEGMUAEGMNNAFIRSYBJKQXZCFCENSTCEIILTCEILPTCGEIPSTDDHNOTDHHLORDHLNORDHLNOREIIITTEMOTTTENSSSUFIPRSYGORRVWIPRRRYNROOTUWOOOTTU'
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}