import React from 'react';
import { shallow } from "enzyme";
import { Invitation } from '../components/home/invitation';
import { Board } from '../components/game-board/board';
import { Result } from '../components/game-board/result'

describe('<Invitation />', () => {
  it('renders an input area', () => {
    const wrapper = shallow(<Invitation />);
    const input = wrapper.find('input');
    expect(input.length).toEqual(1);
  })
})

describe('<Board />', () => {
  it('render boogle board', () => {
    const props = {
      onSelectBoard: jest.fn()
    };
    const wrapper = shallow(<Board {...props} />);
    const exptectedBoard = '<div class="game-box"><div class="row-words board-four"><button class="btn-word word">M</button><button class="btn-word word">R</button><button class="btn-word word">M</button><button class="btn-word word">S</button><button class="btn-word word">T</button><button class="btn-word word">Z</button><button class="btn-word word">S</button><button class="btn-word word">H</button><button class="btn-word word">H</button><button class="btn-word word">O</button><button class="btn-word word">O</button><button class="btn-word word">N</button><button class="btn-word word">E</button><button class="btn-word word">N</button><button class="btn-word word">H</button><button class="btn-word word">R</button></div></div>'

    const mockData = [{ row: 1, col: 1, value: "M", isSelected: false },
    { row: 1, col: 2, value: "R", isSelected: false },
    { row: 1, col: 3, value: "M", isSelected: false },
    { row: 1, col: 4, value: "S", isSelected: false },
    { row: 2, col: 1, value: "T", isSelected: false },
    { row: 2, col: 2, value: "Z", isSelected: false },
    { row: 2, col: 3, value: "S", isSelected: false },
    { row: 2, col: 4, value: "H", isSelected: false },
    { row: 3, col: 1, value: "H", isSelected: false },
    { row: 3, col: 2, value: "O", isSelected: false },
    { row: 3, col: 3, value: "O", isSelected: false },
    { row: 3, col: 4, value: "N", isSelected: false },
    { row: 4, col: 1, value: "E", isSelected: false },
    { row: 4, col: 2, value: "N", isSelected: false },
    { row: 4, col: 3, value: "H", isSelected: false },
    { row: 4, col: 4, value: "R", isSelected: false }]

    wrapper.setState({ boogleBoard: mockData })

    const realBoogleBoard = wrapper.find('div.game-box').html();
    expect(realBoogleBoard.indexOf(exptectedBoard) > -1).toEqual(true);
  })

  it('Should fire submitHandler function', () => {
    const props = {
      onSelectBoard: jest.fn()
    };

    const wrapper = shallow(<Board {...props} />);
    wrapper.instance().submitHandler = jest.fn();
    let { submitHandler } = wrapper.instance();
    expect(submitHandler).toHaveBeenCalledTimes(0);
    const button = wrapper.find('button.submit');
    button.simulate("click");
    expect(button.hasClass('submit')).toBe(true);
    expect(button.text()).toEqual('SUBMIT');
    expect(submitHandler).toHaveBeenCalledTimes(1);
  });
})

describe('<Result />', () => {
  it('should be 0 value for total score on initialization', () => {
    const wrapper = shallow(<Result />);
    const totalScore = wrapper.find('h6.score');
    const result = totalScore.text();
    expect(result).toBe("0");
  })

  it('should be sum of individual word score', () => {
    expect(1 + 2).toBe(3);
  })

})