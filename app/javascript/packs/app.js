/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

import React, { Component } from 'react'
import routes from '../routes';

import "./app.scss";

export class App extends Component {
	render() {
		return (
			<div>
				{routes}
			</div>
		)
	}
}

export default App
