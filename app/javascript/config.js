/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 *
 */

const config = {
  apiBaseUrl: {
    development: 'http://localhost:3000/api'
  }
}
export default config;