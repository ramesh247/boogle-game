/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */
import React, { Component } from 'react'

export class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalScore: 0,
      wordList: [],
      minutes: 2,
      seconds: 0
    }
    this.startTimer = this.startTimer.bind(this);
  }

  startTimer = () => {
    this.myInterval = setInterval(() => {
      const { seconds, minutes } = this.state
      if (seconds > 0) {
        this.setState(({ seconds }) => ({
          seconds: seconds - 1
        }))
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(this.myInterval)
          this.props.onTimeEnd(true);
        } else {
          this.setState(({ minutes }) => ({
            minutes: minutes - 1,
            seconds: 59
          }))
        }
      }
    }, 1000)
  }

  componentWillReceiveProps(props) {
    if (props.wordList !== this.props.wordList) {
      let totalScore = 0;
      props.wordList.map(val => {
        totalScore += val.point
      })
      if (totalScore == 0) {
        this.setState({ minutes: 2, seconds: 0 }, () => {
          clearInterval(this.myInterval)
          this.startTimer();
          this.props.onTimeEnd(false);
        })
      }
      this.setState({ wordList: props.wordList, totalScore: totalScore })
    }
  }

  render() {
    const { minutes, seconds } = this.state
    return (
      <div className="col-lg-6">
        <label className="text-danger text-md text-center" > TIME REMAINING: {minutes}:{seconds < 10 ? `0${seconds}` : seconds} </label>
        <div className="score-box">
          <span className="d-flex justify-content-between"><h6 className="text-muted">WORD</h6>

            <h6 className="text-muted ">SCORE</h6></span>
          <ul className="word-list">
            {
              this.state.wordList.map((word, i) => {
                return (
                  <li key={i}> <span>{word.word.toUpperCase()}</span> <span>{word.point}</span> </li>
                )
              })
            }
          </ul>
          <div className="total-score">
            <h6 className="text-muted">TOTAL SCORE</h6> <h6 className="score">{this.state.totalScore}</h6>
          </div>
        </div>
      </div>
    )
  }
}

export default Result