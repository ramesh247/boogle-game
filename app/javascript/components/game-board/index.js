/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */

import React, { Component } from 'react'
import Board from './board'
import Result from './result'

export class GameBoard extends Component {

	constructor(props) {
		super(props);
		this.state = {
			userName: '',
			wordList: [],
			isTimeEnd: false
		}
	}

	componentDidMount() {
		let params = new URLSearchParams(this.props.location.search);
		this.setState({ userName: params.get('username') ? params.get('username') : '' })
	}

	handleBoard = (wordList) => {
		this.setState({ wordList: wordList })
	}

	onTimeEnd = (isTimeEnd) => {
		this.setState({ isTimeEnd: isTimeEnd })
	}

	componentWillUnmount() {
	}
	
	render() {
		return (
			<div className="container wrapper">
				<div className="row mt-4">
					<Board userName={this.state.userName} onSelectBoard={this.handleBoard} timeStatus={this.state.isTimeEnd} />
					<Result wordList={this.state.wordList} onTimeEnd={this.onTimeEnd} />
				</div>
			</div>
		)
	}
}

export default GameBoard;
