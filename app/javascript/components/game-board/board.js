/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */

import React, { Component } from 'react'
import { generateBoard } from "../../utils/boogleUtility";
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import * as boardService from '../../services/boardService';
import messageConstant from '../../constants/messageConstant';

export class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boogleBoard: [],
      selectedWord: '',
      selectedTextArray: [],
      correctWordList: [],
      isTimeComplete: false
    }
    this.selectHandler = this.selectHandler.bind(this);
    this.resetBoard = this.resetBoard.bind(this);
    this.checkValidSelection = this.checkValidSelection.bind(this);
    this.isAlreadySelected = this.isAlreadySelected.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.validateWord = this.validateWord.bind(this);
    this.handleBoard = this.handleBoard.bind(this);
  }

  componentDidMount() {
    this.resetBoard();
  }

  componentWillReceiveProps(props) {
    this.setState({ isTimeComplete: props.timeStatus })
  }

  checkValidSelection = (data) => {
    let currentRow = this.state.selectedTextArray.length > 0 ? this.state.selectedTextArray[0].row : 0;
    let currentColumn = this.state.selectedTextArray.length > 0 ? this.state.selectedTextArray[0].col : 0;
    if (Math.abs(data.row - currentRow) == 0 && Math.abs(data.col - currentColumn) == 0) {
      return { state: 0 }
    } else if (this.state.selectedTextArray.find(l => { return l.row == data.row && l.col == data.col })) {
      return { state: -1 }
    } else if (this.state.selectedTextArray.length == 0 || (Math.abs(data.row - currentRow) < 2 && Math.abs(data.col - currentColumn) < 2)) {
      return { state: 1 }
    } else {
      return { state: -1 }
    }
  }



  validateWord = (word) => {
    if (!word || word.length < 3) {
      return { isValid: false, message: messageConstant.MINIMUM_CHARACTER_MSG }
    } else if (this.isAlreadySelected(word)) {
      return { isValid: false, message: `${messageConstant.REPEAT_WORD_MSG} ${word.toUpperCase()}` }
    } else {
      return { isValid: true, message: messageConstant.VALID_MSG }
    }
  }

  isAlreadySelected = (word) => {
    let wordObj = this.state.correctWordList.find(x => (x.word == word))
    if (wordObj)
      return true
    else
      return false
  }

  selectHandler = (letter, i) => {
    let responseObj = this.checkValidSelection(letter)
    let word = this.state.selectedWord;
    if (responseObj.state == 1) {
      const newBoards = [...this.state.boogleBoard];
      newBoards[i].isSelected = true
      this.setState(prevState => ({
        selectedTextArray: [letter, ...prevState.selectedTextArray], selectedWord: word + letter.value,
        boogleBoard: newBoards
      }), () => {
      })
    }
    else if (responseObj.state == 0) {
      const newBoards = [...this.state.boogleBoard];
      newBoards[i].isSelected = false
      this.setState({
        selectedWord: word.substring(0, word.length - 1), selectedTextArray: this.state.selectedTextArray.slice(1), boogleBoard: newBoards
      }, () => {
      })

    } else {
      ToastsStore.warning(messageConstant.INVALID_CHARACTER_MSG);
    }
  }

  resetBoard = () => {
    let boardLetter = []
    let array = generateBoard(16).split('');
    array.map((v, i) => {
      let row = i < 4 ? 1 : (i > 3 && i < 8) ? 2 : (i > 7 && i < 12) ? 3 : (i > 11 && i < 16) ? 4 : 0;
      let col = i < 4 ? i + 1 : (i > 3 && i < 8) ? i - 3 : (i > 7 && i < 12) ? i - 7 : (i > 11 && i < 16) ? i - 11 : 0;
      boardLetter.push({ row: row, col: col, value: v, isSelected: false })
    })
    this.setState({
      boogleBoard: boardLetter,
      selectedWord: '',
      selectedTextArray: [],
      correctWordList: [],
      isTimeComplete: false

    }, () => {
      this.handleBoard();
    })
  }

  handleBoard = () => {
    this.props.onSelectBoard(this.state.correctWordList);
  }

  submitHandler = () => {
    let isValidObj = this.validateWord(this.state.selectedWord ? this.state.selectedWord.toLowerCase() : '')
    if (!isValidObj.isValid) {
      ToastsStore.warning(isValidObj.message);
      let selectedTextArray = [...this.state.selectedTextArray];
      selectedTextArray = [];
      let newBoards = [...this.state.boogleBoard];
      newBoards.map(val => {
        val.isSelected = false
      })

      this.setState({
        selectedTextArray: selectedTextArray,
        boogleBoard: newBoards,
        selectedWord: ''
      })
    } else {
      boardService.scores(this.state.selectedWord.toLowerCase()).then(response => {
        if (response.payloads.data.point > 0) {
          ToastsStore.success(`${response.payloads.message} ${response.payloads.data.point}`);
          this.setState(prevState => ({
            correctWordList: [...prevState.correctWordList, { word: this.state.selectedWord.toLowerCase(), point: response.payloads.data.point }]
          }), () => {
          })
        } else {
          ToastsStore.warning(response.payloads.message);
        }

        let selectedTextArray = [...this.state.selectedTextArray];
        selectedTextArray = [];
        let newBoards = [...this.state.boogleBoard];
        newBoards.map(val => {
          val.isSelected = false
        })
        this.setState({
          selectedTextArray: selectedTextArray,
          boogleBoard: newBoards,
          selectedWord: ''
        })

        this.handleBoard();
      }).catch(error => {
        console.log('error', error)
        ToastsStore.error(error);
      })
    }
  }

  render() {
    let props = this.props;
    return (
      <div className="col-lg-6" >
        <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.TOP_RIGHT} />
        <span className="d-flex justify-content-between mb-2 align-items-baseline">
          <button className="btn btn-start text-sm py-1" onClick={() => this.resetBoard()}><i className="fas fa-redo"></i> RESTART</button>
          <p className="player-name mb-0"> PLAYER: {props.userName}</p>
        </span>
        <div className="game-box">
          <div className="row-words board-four">
            {
              this.state.boogleBoard.map((letter, i) => {
                return (
                  <button disabled={this.state.isTimeComplete} key={i} className={letter.isSelected ? 'btn-word word ' + 'word-selected' : 'btn-word word'}
                    onClick={() => this.selectHandler(letter, i)}>{letter.value}</button>
                )
              })
            }
          </div>
        </div>
        <div className="col-lg-12 mt-3 px-0">
          <span className="d-flex justify-content-between">
            <p className="text-muted">CURRENT WORD</p>
            <strong>{this.state.selectedWord}</strong>
          </span>
          {this.state.isTimeComplete ? <button className="btn btn-start mt-2 btn-lg" onClick={() => this.resetBoard()}>PLAY AGAIN</button> :
            <button className="btn btn-start mt-2 btn-lg submit" onClick={() => this.submitHandler()}>SUBMIT</button>
          }
        </div>
      </div>
    )
  }
}

export default Board;
