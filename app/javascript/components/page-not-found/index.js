/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */

import React, { Component } from 'react'

export class PageNotFound extends Component {
	render() {
		return (
			<div>
				<div>
					<h1>The page you were looking for doesn't exist.</h1>
					<p>You may have mistyped the address or the page may have moved.</p>
				</div>
				<p>If you are the application owner check the logs for more information.</p>
			</div>
		)
	}
}

export default PageNotFound
