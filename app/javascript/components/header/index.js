/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 * 
 */

import React, { Component } from 'react';
import logo from "../../assets/images/logo.jpeg";
import {Link} from 'react-router-dom';

export class Header extends Component {

	render() {
		return (
			<div className="header">
				<nav className="navbar navbar-light bg-light" >
					<Link className="navbar-brand d-flex" to="/">
						<img src={logo} width="80" className="d-inline-block align-top" alt="LOGO" />
					</Link>
				</nav>
			</div>
		)
	}
}

export default Header
