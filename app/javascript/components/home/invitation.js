/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */

import React, { Component } from 'react'
import routeConstants from '../../constants/routeConstant';

export class Invitation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userName: '',
			isValidForm: true
		};
		this.onSubmit = this.onSubmit.bind(this);
		this.nameChangeHandler = this.nameChangeHandler.bind(this);
	}

	nameChangeHandler = (event) => {
		this.setState({ userName: event.target.value });
	}

	onSubmit(e) {
		e.preventDefault();
		if (!this.state.userName) {
			this.setState({ isValidForm: false })
			return;
		} else {
			this.setState({ isValidForm: true })

			this.props.history.push({
				pathname: `${routeConstants.PLAYGAME}`,
				search: `?username=${this.state.userName}`
			})
		}
	}

	render() {
		return (
			<div>
				<div class="col-lg-12 mb-2">
					<h5 class="username-label">Enter your name</h5>
				</div>
				<div className="col-lg-12">
					<form id="userForm" onSubmit={this.onSubmit}>
						<input type="text " className="form-control" onChange={this.nameChangeHandler} placeholder="e.g. Ramesh Prajapati" />
						{!this.state.isValidForm ?
							<span className="text-red">Please Enter Your Name</span> : ''}
						<button type="submit" className="btn btn-start mt-3 btn-lg">PLAY YOUR GAME</button>
					</form>
				</div >
			</div>
		)
	}
}


export default Invitation