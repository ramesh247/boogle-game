/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */

import React from 'react'

const Overview = () => {
	return (
		<div className="col-lg-12">
			<h1 className="intro">Boogle Game</h1>
			<p>Boggle is a word game invented by Allan Turoff and originally distributed by Parker Brothers.
				The game is played using a grid of letters, in which the players attempt to find words in sequences of adjacent letters.</p>
		</div>
	)
}

export default Overview