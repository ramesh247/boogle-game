/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */
import React, { Component } from 'react'
import Overview from './overview'
import Invitation from './invitation'

export class Home extends Component {
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<div className="container mt-3 wrapper">
				<Overview />
				<Invitation history = {this.props.history}  />
			</div>
		)
	}
}

export default Home;
