/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
 * 
 */

import React, { Component } from 'react';
import Header from '../header';

class Layout extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <main>
          <div className="p-3">
            {this.props.children}
          </div>
        </main>
      </div>
    );
  }
}

export default Layout;

