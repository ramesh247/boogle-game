/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 * 
 */

const apiConstants = {
  GAME: 'games/scores'
};

export default apiConstants;