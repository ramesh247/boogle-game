/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

const routeConstants = {
  MINIMUM_CHARACTER_MSG: 'Please select atleast 3 characters.',
  REPEAT_WORD_MSG:'You have already selected word',
  VALID_MSG:'You have selected valid word.',
  INVALID_CHARACTER_MSG:'You cannot select that letter.'

};

export default routeConstants;