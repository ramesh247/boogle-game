/**
 * @author Ramesh Prajapati <rameshprajapati247@gmail.com>
 */

const routeConstants = {
  PLAYGAME: '/play-game'
};

export default routeConstants;