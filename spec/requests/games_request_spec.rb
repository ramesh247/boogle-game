require 'rails_helper'

RSpec.describe "Games", type: :request do

  # Test Case 1
  # Description : Validate Correct Words and provide game point
  it "validates a word and returns corresponding game point ", :focus => true do
    get "/api/games/scores/cat"
    expect(response).to have_http_status(:ok)
    data = JSON.parse(response.body)
    expect(data["payloads"]["status"]).to eq(200)
    expect(data["payloads"]["data"]["point"]).to eq(1)
    expect(data["payloads"]["message"]).to eq('You have scored')
  end

  # Test Case 2
  # Description : Invalid Word
  it "should mark the word as incorrect and provide 0 point.", :focus => true do
    get "/api/games/scores/ramesh"
    expect(response).to have_http_status(:ok)
    data = JSON.parse(response.body)
    expect(data["payloads"]["status"]).to eq(200)
    expect(data["payloads"]["data"]["point"]).to eq(0)
    expect(data["payloads"]["message"]).to eq('Please enter correct word')
  end

  # Test Case 3
  # Description : Minimum length word
  it "should mark the word as invalid and provide null point.", :focus => true do
    get "/api/games/scores/ca"
    expect(response).to have_http_status(:ok)
    data = JSON.parse(response.body)
    expect(data["payloads"]["status"]).to eq(200)
    expect(data["payloads"]["data"]["point"]).to eq(0)
    expect(data["payloads"]["message"]).to eq('Please enter atleast 3 characters.')
  end

end
