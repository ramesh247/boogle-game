#  @author Ramesh Prajapati <rameshprajapati247@gmail.com> 
Rails.application.routes.draw do
  namespace :api, defaults: { format: "json" } do
    get "games", to: "games#index"
    get "games/scores/:keyword", to: "games#scores"
  end

  root 'home#index'

  get "*page", to: "home#index", constraints: ->(req) do
  !req.xhr? && req.format.html?
end
end
